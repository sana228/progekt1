﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {

	 int BallJumpPower = 5000;
	int BallMovePower = 1;
	 Rigidbody Ball;


	 void Start () {
		Ball = this.gameObject.GetComponent<Rigidbody>(); 
		Ball.mass = 10;
	}

	void Update () {

		if (Input.GetKey (KeyCode.D))
			Ball.AddForce (Vector3.right * BallMovePower);

		if (Input.GetKey (KeyCode.W))
			Ball.AddForce (Vector3.forward * BallMovePower);
	
		if (Input.GetKey (KeyCode.S))
			Ball.AddForce (Vector3.back * BallMovePower);
		
		if (Input.GetKey (KeyCode.A))
			Ball.AddForce (Vector3.left * BallMovePower);
		
		if (Input.GetKeyDown (KeyCode.Space))
			Ball.AddForce (Vector3.up * BallJumpPower);

	}
}